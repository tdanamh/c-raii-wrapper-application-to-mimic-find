#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <string>

using namespace std;

class RaiiWrapper {

protected:
    WIN32_FIND_DATA FindFileData;
    HANDLE hFind;

public:
    RaiiWrapper(int argc, TCHAR* argv[]) {
        _tprintf(TEXT("Target file is %s\n"), argv[1]);
        hFind = FindFirstFile(argv[1], &FindFileData);
    }
    ~RaiiWrapper() {
        CloseHandle(hFind);
    }
    void search() {
        if (hFind == INVALID_HANDLE_VALUE) {
            printf("FindFirstFile  failed(%d)\n", GetLastError());
            return;
        }
        else {
            _tprintf(TEXT("The first file found is %s\n"), FindFileData.cFileName);
            int success;
            while (success = FindNextFile(hFind, &FindFileData)) {
                _tprintf(TEXT("The next file found is %s\n"), FindFileData.cFileName);
            }
            FindClose(hFind);
        }
    }
};

void _tmain(int argc, TCHAR* argv[])
{

    if (argc != 2)
    {
        _tprintf(TEXT("Usage: %s [target_file]\n"), argv[0]);
        return;
    }

     RaiiWrapper *find = new RaiiWrapper(argc, argv);
     find->search();
    
}


